# Prueba tecnica Front Ecomsur 2021
## Freddy Perez - Frontend Developer

### Prueba desarrollada con React Redux.

  - [Requerimientos mínimos](#requerimientos-mínimos)
  - [Instalar y Correr la aplicación](#instalar-y-correr-la-aplicación)
  

## Requerimientos mínimos

Node 14.15.0

## Instalar y Correr la aplicación

Instalar API (backend) y la aplicacion React (front):

1. En la carpeta `root` de la aplicacion correr:
   `npm install`
2. Navega al directorio `front` y vuelve a correr el comando:
   `npm install`
3. Regresa al directorio root `cd ..`.

La aplicación está compuesta de un servidor Express y una instalación básica de Create-React-App (CRA). Todo está configurado para correr con un solo comando

`npm run dev`

Esto correrá ambas aplicaciones (Express y CRA) al mismo tiempo.

- El home se encuentra en:
  `http://localhost:3000/`

- la lista de productos (PLP) esta en:
  `http://localhost:3000/products`

- La pagina de detalle del producto (PDP) esta en:
  `http://localhost:3000/products/1`

- El carrito se encuentra en:
  `http://localhost:3000/cart`



