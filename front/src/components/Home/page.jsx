import Slider from '../Slider'
import ProductList from '../ProductList'

const Page = () => {
    return(
        <>
        <div className="main-container">
            
            <Slider />
            <ProductList />
            
        </div>
            
        </>
    )
}

export default Page