import Footer from '../Templates/Footer'
import Navbar from '../Templates/Navbar'
import Page from './page'

const Home = () => {
    return (
        <>
        <Navbar />
        <Page />
        <Footer />
        </>
    )
}

export default Home