import { Link , useHistory} from "react-router-dom"
import BtnAddCart from "../Templates/BtnAddCart"
import RatingStar from "../Templates/RatingStar"
import Reviews from "../Templates/Reviews"

const Page = ({product}, index) => {
    const history = useHistory()
    return (
        <>
        <div key={index} className="dealcard">
            <div className="header" onClick={()=>{history.push('/product/' + product._id)}}>
                <img src={'http://localhost:5000/'+ product.image} alt="" />
                <div className="review-container">
                    <Reviews review={product.numReviews} />
                </div>
            </div>
            <div className="body">
                <div className="title">
                    <div className="brand text-overline">
                        {product.brand}
                    </div>
                    <div className="name text-h5">
                        {product.name}
                    </div>
                    <div className="rating">
                        <span>({product.rating % 1 === 0 ? `${product.rating}.0` : product.rating})</span>
                        <RatingStar rating={product.rating}/>
                    </div>
                    
                </div>
                <div className="price text-body">
                    US${product.price}
                </div>
                
            </div>
            <div className="footer">
                <BtnAddCart product={product}/>
                <Link to={'/product/' + product._id} className="btn btn-details">Ver detalles</Link>
            </div>
        </div>
        </>
    )
}

export default Page