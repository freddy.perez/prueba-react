import Page from './page'

import './styles.css'

const DealCard = ({product}) => {
    return (
        <Page product={product} />
    )
}

export default DealCard