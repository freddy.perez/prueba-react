import NavBar from '../Templates/Navbar'
import ProductList from '../ProductList'
import Footer from '../Templates/Footer'

const Page = ()=>{
    

    return (
        <>
            <NavBar />
            <div className="main-container">
            <ProductList />
            </div>
            
            <Footer />
        </>
    )
}

export default Page