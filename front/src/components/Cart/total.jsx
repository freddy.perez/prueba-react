import { useSelector } from "react-redux"
import { useHistory } from "react-router"
import Swal from "sweetalert2"

import imageSaludo from './saludo.jpg'

const Total = ()=>{
    const { total } = useSelector(state => state.cart)
    const history = useHistory()
    return (
        <div>
            <div className="cart-total-container">
                <div className="total-item">
                    <div className="text">Subtotal:</div>
                    <div className="price">US${total.toFixed(2)}</div>
                </div>
                <div className="total-item">
                    <div className="text">Envio:</div>
                    <div className="price">US$40</div>
                </div>
                <div className="total-item total-final">
                    <div className="text">Total:</div>
                    <div className="price">US${(total + 40).toFixed(2)}</div>
                </div>
            </div>
            <button className="checkout-btn" onClick={()=>{saludoSimpatico()}}>
                <i className="fa fa-shield-alt"></i>
                Finaliza tu compra</button>
            <button className="seguir-comprando-btn" onClick={()=>{history.push('/')}}>Seguir comprando</button>
        </div>
    )
}


const saludoSimpatico = ()=>{
    Swal.fire({
        html:`<img style="width:100%" src="${imageSaludo}" />`,
        confirmButtonText:'Muchas Gracias Ecomsur!'
    })
}

export default Total