import Page from './page'
import Navbar from '../Templates/Navbar'
import Footer from '../Templates/Footer'

import './styles.css'

const Cart = ()=>{
    return (
        <>
        <Navbar />
        <Page />
        <Footer />
        </>
    )
}

export default Cart