import { useSelector } from "react-redux"
import { useHistory } from 'react-router-dom'

import Swiper from 'swiper/swiper-bundle'
import 'swiper/swiper-bundle.css'
import { useEffect } from "react"
import Total from "./total"
import CartItem from "./cartItem"




const Page = () => {
    useEffect(() => {
        swiperCart()
    })
    const { products } = useSelector(state => state.cart)

    const history = useHistory()


    //console.log(products)
    return (
        <div className="main-container">
            <h1>Carrito</h1>
            <div className="cart">
                
                <div className="cart-container swiper-container">
                    <div className="wrapper swiper-wrapper">
                        {
                            products.length > 0 ?
                                products.map((product, i) =>
                                    (<CartItem product={product} key={i} />)
                                ) :
                                (
                                    <div className="empty-cart">
                                        <div className="title">El carrito esta vacio</div>
                                        <button className="btn" onClick={() => { history.push('/products') }}>Ver productos</button>
                                    </div>
                                )
                        }

                    </div>
                    <div className="swiper-pagination"></div>
                </div>
                {
                    products.length > 0 && (<Total />) 
                }
                
            </div>

            
        </div>
    )
}

const swiperCart = () => {
    // eslint-disable-next-line
    let swiper = new Swiper('.cart-container', {
        speed: 500,
        pagination: {
            el: '.swiper-pagination'
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 8
            },
            610: {
                slidesPerView: 3,
                spaceBetween: 8
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 12
            },
            1440: {
                slidesPerView: 3,
                spaceBetween: 16
            }
        }
    })
}


export default Page