import { useState } from "react"
import { useDispatch } from "react-redux"
import { addToCartByQty, removeFromCart } from "../../store/slices/cart"
import Swal from 'sweetalert2'

const CartItem = ({product}) => {

    const dispatch = useDispatch()
    
    const [inputValue, setInputValue] = useState(product.qty.toString())

    const remFromCart = (product)=>{
        Swal.fire({
            title: 'Eliminar Producto!',
            text: "Desea remover este producto del carrito?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, por favor!',
            cancelButtonText:'No, Gracias'
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire(
                'Confirmado!',
                'El producto fué sacado del carrito.',
                'success'
              )
              dispatch(removeFromCart(product))
            }
          })
    }

    return (
        <div className="cart-card swiper-slide">
            <div className="img-container">
                <img src={'http://localhost:5000' + product.image} alt="" />
                <span onClick={() => { remFromCart(product) }}>&times;</span>
            </div>
            <div className="body">
                <div className="brand text-overline">
                    {product.brand}
                </div>
                <div className="title text-body">
                    {product.name}
                </div>
                <div className="price text-h5">
                    US${product.price}
                </div>
                
            </div>
            <div className="actions">
                <button onClick={()=>{
                    if(product.qty > 1){
                        dispatch(addToCartByQty({product, qty:product.qty - 1}))
                        setInputValue(product.qty - 1)
                    }else{
                        dispatch(addToCartByQty({product, qty:1}))
                        setInputValue(1)
                    }
                    
                    }}>-</button>
                <input type="text" 
                    value={inputValue} min="1" max={product.countInStock}
                    onBlur={(e) => {
                        if(!e.target.value){
                            e.target.value = product.qty
                        }
                        if(e.target.value > product.countInStock){
                            e.target.value = product.countInStock
                        }
                        if(e.target.value <= 0){
                            e.target.value = 1
                        }
                    }} 
                    onChange={(e) => { 
                        if(e.target.value) {
                            setInputValue(e.target.value)
                            dispatch(addToCartByQty({ product, qty: parseInt((e.target.value)) })) 
                        }else{
                            setInputValue('')
                        }
                    }} />
                <button onClick={()=>{
                    if(product.qty < product.countInStock){
                        dispatch(addToCartByQty({product, qty: product.qty + 1}))
                        setInputValue(product.qty + 1)
                    }else{
                        dispatch(addToCartByQty({product, qty: product.countInStock}))
                        setInputValue(product.countInStock)
                    }
                    
                    }}>+</button>
            </div>
            <div className="total">
                <div>Total</div>
                <div className="total-product">
                    US${(product.price * product.qty).toFixed(2)}
                </div>
            </div>
        </div>
    )
}

export default CartItem