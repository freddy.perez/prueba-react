import { useParams } from 'react-router'
import Page from './page'
import Navbar from '../Templates/Navbar'
import Footer from '../Templates/Footer'
import './styles.css'

const ProductDetailPage = () =>{
    const {id} = useParams()
    return (
        <>
        <Navbar />
        <Page id={id}/>
        <Footer />
        </>
    )
}

export default ProductDetailPage