import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { fetchProduct } from "../../store/slices/getProduct"
import BtnAddCart from "../Templates/BtnAddCart"
import  RatingStar  from '../Templates/RatingStar'


const Page = ({id})=>{
    const {product}  = useSelector(state => state.product)
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(fetchProduct(id))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[dispatch])

    return (
        <div className="bg-pdp">
            <div className="product-container main-container">
                <div className="header">
                    <img src={'http://localhost:5000'+product.image} alt="" />
                </div>
                <div className="body">
                    <div className="brand text-overline">
                        {product.brand}
                    </div>
                    <div className="title text-h5">
                        {product.name}
                    </div>
                    <div className="rating">
                        <RatingStar rating={product.rating}/>
                    </div>
                    <div className="reviews">
                        <i className="fa fa-eye"></i>&nbsp;
                        {product.numReviews} Vistas
                    </div>
                    <div className="price text-body">
                        US${product.price}
                    </div>
                    <BtnAddCart product={product}/>
                    <h3>Descripción</h3>
                    <p>{product.description}</p>
                </div>
                <div className="footer">
                    
                </div>
            </div>
        </div>
    )
}

export default Page