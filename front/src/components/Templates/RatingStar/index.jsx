import './styles.css'
const RatingStar = ({rating})=>{
    return(
        <>
            <div className="rating-container">
                {
                    [...Array(5)].map((elem, index) =>
                        index < rating && index + 1 > rating ?
                        <i key={index} className="fas fa-star-half-alt"></i> :
                        index < rating ? 
                        <i key={index} className="fa fa-star"></i> :
                        <i key={index} className="far fa-star"></i>
                    )
                }
            </div>
        </>
    )
}

export default RatingStar