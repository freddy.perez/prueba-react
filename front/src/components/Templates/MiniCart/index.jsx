import { useDispatch, useSelector } from 'react-redux'
import { openCart } from '../../../store/slices/offCanvasCart'
import './styles.css'


const MiniCart = ()=>{

    const {count} = useSelector(state => state.cart)
    const dispatch = useDispatch()

    return (
        <>
        <div className="cart-counter" onClick={()=>{dispatch(openCart())}}>
            <i className="fas fa-shopping-cart"></i>
            <span className="badge">{count || 0}</span>
        </div>
        </>
    )
}

export default MiniCart