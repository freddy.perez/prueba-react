import { addToCart } from "../../../store/slices/cart";
import { useDispatch, useSelector } from "react-redux";
import "./styles.css";
import { useState } from "react";
import Swal from 'sweetalert2'
import { useEffect } from "react";

const BtnAddCart = ({ product }) => {
	const dispatch = useDispatch();
	const [maxProduct, setMaxProduct] = useState(false)
	const productsCart = useSelector(state => state.cart)
	console.log(productsCart.products)
	const prodInCart = productsCart.products.find(e => e._id === product._id)
	console.log(prodInCart)


	useEffect(() => {
		if (prodInCart && prodInCart.qty >= prodInCart.countInStock) {
			setMaxProduct(true)
		}else if(product.countInStock === 0){
			setMaxProduct(true)
		}else{
			setMaxProduct(false)
		}
	}, [prodInCart, product])

	const [thanks, setThanks] = useState(false);

	const animateBtn = () => {
		setThanks(true)
		setTimeout(() => {
			setThanks(false)
		}, 2000)
		Swal.fire(
			'Muchas gracias',
			'Tu producto ha sido agregado al carrito',
			'success'
		)
	}

	return (
		<>
			<button
				className={`btn btn-add-cart ${maxProduct ? 'disableded':''}`}
				onClick={() => {
					dispatch(addToCart(product))
					animateBtn()
				}}
			 >
				<span className={'add-cart-icon ' + (thanks ? 'active' : '')}>
					<i className="fa fa-check"></i>
					<span>Gracias</span>
				</span>

				<span className={"add-cart-icon " + (thanks || maxProduct ? '' : 'active')}>
					<i className="fa fa-shopping-cart"></i>
					<span>Agregar</span>
				</span>
				<span className={"add-cart-icon " + (maxProduct ? 'active' : '')}>
				
					<span>Sin Stock</span>
				</span>
			</button>
		</>
	);
};

export default BtnAddCart;
