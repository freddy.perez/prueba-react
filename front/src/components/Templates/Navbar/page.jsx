import { useHistory } from "react-router"
import MiniCart from "../MiniCart"
import OffCanvasCart from "../offCanvasCart"

const Page = ()=>{
    const history = useHistory()
    return (
        <div className="navbar-container">
            <div className="navbar">
                <div className="offcanvas-container">
                    <i className="fas fa-bars"></i>
                </div>
                <h3 onClick={()=>{history.push('/')}}>Ecomtienda</h3>
                <MiniCart />
            </div>
            <OffCanvasCart />
        </div>
    )
}

export default Page