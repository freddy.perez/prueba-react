import { useDispatch } from "react-redux"
import { removeFromCart } from "../../../store/slices/cart"

const OffCanvasDealcard = ({product}) => {
    const dispatch = useDispatch()
    return (
        <>
            <div className="oc-dealcard">
                <img src={`http://localhost:5000${product.image}`} alt="" />
                <div className="info">
                    <div className="name">{product.name}</div>
                    <div className="price">US${product.price}</div>
                </div>
                <span className="close" onClick={()=>{dispatch(removeFromCart(product))}}>&times;</span>
            </div>
        </>
    )
}

export default OffCanvasDealcard