import { useDispatch, useSelector } from "react-redux"
import OffCanvasDealcard from "./offcanvasDealcard"
import { closeCart } from "../../../store/slices/offCanvasCart"
import './styles.css'
import { useHistory } from 'react-router-dom'


const OffCanvasCart = () => {
    const offcanvas = useSelector(state => state.offCanvasCart)
    const dispatch = useDispatch()
    const { products, total } = useSelector(state => state.cart)
    const history = useHistory()

    //console.log(offcanvas)
    return (
        <>
            <div className="offcanvas offcanvas-right" style={{
                width: offcanvas.width,
                padding: offcanvas.padding
            }}>
                <button className="closebtn" onClick={() => { dispatch(closeCart()) }}>&times;</button>
                <h4>Productos en el carrito</h4>
                {
                    products.map((e, i) => (<OffCanvasDealcard product={e} key={i} />))
                }

                {
                    products.length > 0 ?
                        (
                            <>
                                <h3>Total: US${total}</h3>
                                <button className="add-cart-btn-off-canvas" onClick={() => {
                                    history.push('/cart')
                                    dispatch(closeCart())
                                }}>
                                    <i className="fa fa-shopping-cart"></i>
                                    &nbsp;
                                    <span>Ir a pagar</span>
                                </button>
                            </>
                        ):(
                            <>
                                <div className="cart-empty">
                                    No hay productos en el carrito
                                </div>
                            </>
                        )
                    }
                    <button style={{width:'250px'}} className="checkout-btn" style={{background:'var(--color-primary)'}} onClick={
                        ()=>{dispatch(closeCart())}
                    }>Seguir comprando</button>
            </div>
            <div
                className={`overlay ${offcanvas.width !== '0' ? 'active' : ''}`}
                onClick={() => { dispatch(closeCart()) }}></div>
        </>

    )

}







export default OffCanvasCart