import DealCard from "../DealCard"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react"
import { fetchAllProducts } from "../../store/slices/loadAllResults"

const Page = ()=>{
    const {list: products} = useSelector(state => state.products)
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(fetchAllProducts())
    },[dispatch])

    return (
        <>
        <div className="dealcard-container">
            {
                products.map((product,index)=>(
                    <DealCard product={product} key={index}/>
                ))
            }
        </div>
        </>
    )
}

export default Page