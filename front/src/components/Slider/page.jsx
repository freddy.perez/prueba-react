import { useEffect } from 'react'
import Swiper from 'swiper/swiper-bundle'

import 'swiper/swiper-bundle.css'
import BP1 from './bp1.jpg'
import BP1mobile from './bp1-mobile.jpg'
import BP2 from './bp2.jpg'
import BP2mobile from './bp2-mobile.jpg'

const Page = ()=>{

    useEffect(()=>{
        initSlider()
    })
    return (
        <>
            <div className="swiper-container swiper-home">
                <div className="swiper-wrapper">
                    <div className="swiper-slide">
                        <picture>
                            <source srcSet={BP1} media="(min-width:40em)"/>
                            <img src={BP1mobile} alt="" />
                        </picture>
                        
                    </div>
                    <div className="swiper-slide">
                        <picture>
                            <source srcSet={BP2} media="(min-width:40em)"/>
                            <img src={BP2mobile} alt="" />
                        </picture>
                    </div>
                </div>
            </div>
        </>
    )
}

const initSlider = ()=>{
    // eslint-disable-next-line
    let swiper = new Swiper('.swiper-home',{
        autoplay:{
            delay:2000,
            disableOnInteraction:false
        },
        speed:2000
    })
}


export default Page