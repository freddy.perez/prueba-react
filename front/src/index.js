import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import PLP from './components/ProductListPage'
import PDP from './components/ProductDetailPage'
import Home from './components/Home'
import Cart from './components/Cart'
import { Provider } from 'react-redux'
import store from './store'

const Root = (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/products" component={PLP} />
        <Route path="/product/:id" component={PDP} />
        <Route path="/cart" component={Cart} />
        <Route path="/" exact component={Home} />
      </Switch>
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(Root,document.getElementById('root'))
