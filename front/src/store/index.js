import { configureStore } from '@reduxjs/toolkit'
import products from './slices/loadAllResults'
import product from './slices/getProduct'
import cart from './slices/cart'
import offCanvasCart from './slices/offCanvasCart'

export default configureStore({
    reducer:{
        products,
        product,
        cart,
        offCanvasCart,
    }
})
