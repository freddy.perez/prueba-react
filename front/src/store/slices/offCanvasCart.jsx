import { createSlice } from "@reduxjs/toolkit";

export const offcanvasCart = createSlice({
    name:'offCanvasCart',
    initialState:{
        width:'0',
        padding:'0'
    },
    reducers:{
        openCart: (state, action)=>{
            state.width = '280px'
            state.padding = '50px 16px 16px'
        },
        closeCart: (state, action)=>{
            state.width = '0'
            state.padding = '0'
        }
    }
})

export default offcanvasCart.reducer
export const {openCart, closeCart} = offcanvasCart.actions