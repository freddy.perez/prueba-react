import { createSlice } from "@reduxjs/toolkit"
import axios from "axios"

export const getProduct = createSlice({
    name:'product',
    initialState:{
        product:{}
    },
    reducers:{
        setProduct:(state, action)=>{
            state.product = action.payload
        }
    }
})

export default getProduct.reducer

export const {setProduct} = getProduct.actions

export const fetchProduct = (id) => (dispatch) => {
    //console.log('id de fetch:', id)
    axios.get('http://localhost:5000/api/products/' + id)
    .then(response => {
        console.log(response)
        dispatch(setProduct(response.data))
    }).catch(error => console.log(error))
}