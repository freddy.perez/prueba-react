import { createSlice } from '@reduxjs/toolkit'

import axios from 'axios'

export const resultsSlice = createSlice({
    name:'products',
    initialState:{
        list:[]
    },
    reducers:{
        setProductList: (state, action) =>{
            state.list = action.payload
            state.list.forEach(e=>{
                e.qty = 0
            })
        }
    }
})

export default resultsSlice.reducer

export const {setProductList} = resultsSlice.actions

export const fetchAllProducts = () => (dispatch) => {
    axios.get('http://localhost:5000/api/products')
    .then((response) =>{
        //console.log('response', response)
        dispatch(setProductList(response.data))
    }).catch((error) => console.log(error))
}