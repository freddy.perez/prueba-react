import { createSlice } from "@reduxjs/toolkit"

const storage = JSON.parse(localStorage.getItem('cart'))

export const cart = createSlice({
    name:'cart',
    initialState:{
        count: storage ? storage.count : 0,
        products: storage ? storage.products : [],
        total: storage ? storage.total : 0
    },
    reducers:{
        addToCart: (state, action) => {
            if(state.products.some(e => e._id === action.payload._id)){
                state.products.find(e => e._id === action.payload._id).qty++ 
            }else{
                state.products = [...state.products, {...action.payload, qty: 1}]
            }
            updateCart(state)
        },
        removeFromCart: (state, action) => {
            state.products = state.products.filter(e => e._id !== action.payload._id)
            updateCart(state)
        },
        addToCartByQty:(state, action)=>{
            let { qty, product } = action.payload

            let stateProduct = state.products.find(e => e._id === product._id)

            if(qty > product.countInStock){
                stateProduct.qty = product.countInStock
            }else{
                if(qty > 0){
                    stateProduct.qty = qty
                }else{
                    stateProduct.qty = 1
                }
            }
            updateCart(state)
        }
    }
})

const updateCart = (state)=>{
    state.total = state.products.reduce((acc, el) => {
        acc += el.price * el.qty
        return acc
    },0)
    state.count = state.products.reduce((acc, el) => {
        acc += el.qty
        return acc
    },0)
    localStorage.setItem('cart', JSON.stringify({products: state.products, count:state.count, total:state.total}))
}

export default cart.reducer

export const {addToCart, removeFromCart, addToCartByQty} = cart.actions