# Bitácora de desarrollo

## Planificación
Lo primero fue recopilar toda la información de la prueba y decidir cuantos componentes tendria mi app.
Dibujé en una pizarra un wireframe del primer prototipo de mi prueba y decidi crear un carrusel en la parte superior y cards para mostrar los productos. Con esto pude desglosar mi app en distintos componentes y al mismo tiempo esos componentes subdividirlos en componentes mas pequeños (atomic design).
A continuación desarrollé un flowchart con los componentes principales (pages) y los nodos correspondientes, de acuerdo al proceso anterior. Ya con esto comencé con el siguiente paso

## Estructura
Con mi arbol de componentes listo, continue a clonar el repo y comenzar a crear las diferentes carpetas que iban a componer mi app. Aca tomé una mala decision, ya que no separé las carpetas de paginas en una carpeta llamada "pages", si no que separe los componentes reutilizables en una carpeta llamada templates, lo cual no es incorrecto pero puede ser un poco confuso dependiendo de la forma de desarrollo de cada developer. Una vez terminada la arquitectura básica continue con lo siguiente...

## React Redux with redux toolkit
Aca me tomé el tiempo de investigar, pero me encontre con 1000 realidades distintas en distintos periodos de tiempo, por lo tanto comence un poco confundido, pero gracias a la ayuda de mi hijo (que tambien codea :O ) conocí una herramienta que simplificaba el entorno de redux a solo un archivo store y slices, y además integraba el puglin de redux automaticamente, estamos hablando de redux toolkit, un poderoso hook que me simplificó tanto la curva de aprendizaje como la implementación de un store en mi app.

## Lógica del carrito
Para la lógica del carrito me tuve que auxiliar con google y nuevamente mi hijo (oh my gosh :O ) y términe aprendiendo nuevas maneras de aplicar ternarios multiples, ademas de perfeccionar el entendimiento de los slices y el store. Aca descubrí lo fácil que es crear un componente y validarlo en el momento que se necesita, y lo legible que es el código, al ser modulado.


## Estilos
Ya una vez terminada la lógica, comencé a aplicar estilos en toda la app, comenzando por los colores, los cuales fueron aplicados a través de variables en el root css, por lo tanto si gustan se puede cambiar el color de la app desde los var().
Apliqué el concepto de mobile first, creando primero el diseño para mobile y luego adaptarlo a desktop.

## Últimas correcciones
Comencé por hacer un último testing a mi app y fui encontrando distintos bugs pero menores, los cuales los resolvi googleando un poco y usando los conocimientos que la misma prueba me proporcionó. Además dejamos preparados el entorno para un deploy, en caso de ser necesario

# Conclusión
Me gustó mucho la prueba, puso mis habilidades en juego y eso siempre es bueno. Nunca quedarse con lo que uno cree que sabe o que estudió ya que la tecnología va cambiando muy rápidamente (me paso con redux toolkit). Como dirian los españoles "me ha dejado flipando". React es una herramienta poderosa y que bueno que aprendamos a sacarle provecho.

Muchas gracias.
Freddy Perez.